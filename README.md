O sistema seria desenvolvido utilizando o framework wicket, com hibernate e Banco de dados MySQL. 
Foram escolhidas estas tecnologias, pois foram as que mais tive experiência trabalhando.
A opção pt/en seria desenvolvida utilizando dois arquivos properties com as mesmas chaves e ao selecionar pt ou en seria escolhido o arquivo properties referente a opção escolhida. 
Como esta opção estaria disponível em todas as páginas, foi pensado em criar um panel que seria implementado em todas as páginas. 
Este panel teria uma combo com as opções inglês e português e um método que mudaria o idioma. 
Por padrão seria inicializado em português e as opções da combo seriam Inglês e Português, ao escolher a Opção Inglês, além de modificar o idioma, as opções da combo também seriam modificadas para English e Portuguese.
Para acessar a página de pedido e demais páginas, seria necessário logar no sistema como administrador na primeira vez. 
Foi pensado que o administrador poderia acessar uma página para cadastrar um cliente. O cliente, após cadastrado poderia solicitar um pedido. 
Administrador e cliente seriam tipos de usuários. 
A entidade usuário teria login, senha, nome, endereço e tipo de usuário. 
Sendo que os atributos endereço e nome seriam preenchidos apenas para o tipo cliente. 
Seria adicionado um registro na tabela usuário com o login administrador, a senha admin e o tipo Administrador, não sendo obrigatório informar o nome e nem o endereço. 
Os atributos nome e endereço da entidade usuário seriam apenas obrigatórios para o tipo de usuário cliente.
No método de autenticação seria verificado se o login e a senha são as mesmas cadastradas.
Seriam 4 páginas, uma de login, uma página home, uma de solicitação do pedido e outra de confirmação do pedido. 
Sendo que haveria uma página base com menu, esta página seria estendida para as páginas home, solicitação de pedido e confirmação de pedido. 
A página de login não estenderá está página.
A página base terá menu, cabeçalho e rodapé.
Ao acessar a página login, o usuário informaria login, senha e tipo de usuário. 
Ao passar pelo método de autenticação o usuário seria redirecionado para a página de login. 
Caso seja um administrador, ele teria apenas a opção de cadastrar um cliente. 
Se fosse um cliente, ele teria a opção de solicitar um pedido. 
Após criar um cliente, o administrador é redirecionado para a página home. 
Após solicitar um pedido, o cliente é redirecionado para a página de confirmação do pedido. 
Após confirmar o pedido, o cliente é redirecionado para a tela home. 
O administrador só pode ser adicionado via Banco de dados. 
Caso seja de interesse da Lucene Lanches, poderiam ser adicionadas as opções de alterar e excluir cliente.
Os perfis administrador e cliente são tipos da entidade usuário.